#import ez_setup
#ez_setup.use_setuptools()
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize

ldirs = []
ddirs = []

# Make sure to compile the rust binary with the -soname!!!
# export RUSTFLAGS="-Clink-arg=-Wl,-soname=sfc_ffi.so"; cargo build --release

extensions = Extension("sfc._sfc",
                    sources=["src/sfc/_sfc.pyx"],
                    libraries=["sfcrs"],
                    depends=ddirs,
                    include_dirs=[],
#                    library_dirs=['target/release/'],
#                    runtime_library_dirs=["$ORIGIN/../../target/release/",],
                    library_dirs=['/home/martijn/Documents/work/2017-09_sfclib/rust-sfc/target/release/'],
                    runtime_library_dirs=['/home/martijn/Documents/work/2017-09_sfclib/rust-sfc/target/release/'],
                    extra_compile_args=[],
                    extra_link_args=['-Wl,--verbose']
)

setup(
    name = 'sfc',
    package_dir = {'': 'src'},
    packages = ['sfc',],
    ext_modules = cythonize([extensions,]),
)
