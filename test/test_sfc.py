from range_pack import filter_ranges_by_gapsize

def test_lengthy(maxside, encode, decode):
    """Test that encode and decode functions agree with each other,
    also in higher dims"""
    # 2D
    for x in xrange(maxside):
        for y in xrange(maxside):
            c = (x, y)
            e = encode(c)
            d = decode(e, 2)
            assert tuple(c) == tuple(d), "{} vs {}".format(c, d)
    # 3D
    for x in xrange(maxside):
        for y in xrange(maxside):
            for z in xrange(maxside):
                c = (x, y, z)
                e = encode(c)
                d = decode(e, 3)
                assert tuple(c) == tuple(d), "{} vs {}".format(c, d)
    # 4D
    for x in xrange(maxside):
        for y in xrange(maxside):
            for z in xrange(maxside):
                for t in xrange(maxside):
                    c = (x, y, z, t)
                    e = encode(c)
                    d = decode(e, 4)
                assert tuple(c) == tuple(d), "{} vs {}".format(c, d)
    # 5D
    for x in xrange(maxside):
        for y in xrange(maxside):
            for z in xrange(maxside):
                for t in xrange(maxside):
                    for s in xrange(maxside):
                        c = (x, y, z, t, s)
                        e = encode(c)
                        d = decode(e, 5)
                    assert tuple(c) == tuple(d), "{} vs {}".format(c, d)

def mainlengthy():
    maxsize = 2**5
#    from _sfc import hencode as encode, hdecode as decode
    from _sfc import nencode as encode, ndecode as decode
    print maxsize
    test_lengthy(maxsize, encode, decode)


def main():
    import pprint
    from _sfc import hquery, nquery
    depth = 15
    print 'hi'
    low, high = [1066052, 1642769, 1899], [1083529, 1677722, 4260]
    ranges = hquery(low, high, depth)
#    pprint.pprint( ranges )
    print 'ih', len(ranges), len(filter_ranges_by_gapsize(ranges))
    ranges = nquery(low, high, depth)
#    pprint.pprint( ranges )
    print 'ih', len(ranges), len(filter_ranges_by_gapsize(ranges))

if __name__ == "__main__":
    main()
    mainlengthy()
