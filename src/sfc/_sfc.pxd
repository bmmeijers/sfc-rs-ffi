cdef extern from "sfcrs.h":
    struct Array:
        void* data
        size_t len

    cdef void array_free(Array arr);

    cdef Array hdecode(size_t input, size_t dims);
    cdef size_t hencode(Array input);

    cdef Array ndecode(size_t input, size_t dims);
    cdef size_t nencode(Array input);

    cdef Array hquery(Array low, Array High, size_t maxdepth);
    cdef Array nquery(Array low, Array High, size_t maxdepth);
