#cython: infer_types=True, boundscheck=False

"""
"""
from cython.view cimport array

cimport cython
from libc.stdlib cimport malloc, free

from _sfc cimport (
    Array,
    hdecode as rs_hdecode,
    hencode as rs_hencode,
    nencode as rs_nencode,
    ndecode as rs_ndecode,
    hquery as rs_hquery,
    nquery as rs_nquery,
    array_free as rs_array_free
)

# FIXME:

# hencode
# hdecode

# nencode
# ndecode

# hquery
# nquery

#def do(n):
#    cdef Array result = produce_array(n, 3)
#    cdef size_t* incoming_ptr = <size_t*>(result.data)
#    cdef size_t[:, :] view = <size_t[:result.len,:1]>incoming_ptr
#    val = [view[i, 0] for i in range(result.len)]
##    print view[3, 0]
#    array_free(result)
#    return val

cdef inline Array _convert(seq):
    cdef size_t i
    cdef Array cp
    cdef size_t *my_ints
    my_ints = <size_t *>malloc(len(seq) * cython.sizeof(size_t))
    if my_ints is NULL:
        raise MemoryError()
    for i in range(len(seq)):
        my_ints[i] = seq[i]
    cp.data = <void *>my_ints
    cp.len = len(seq)
    #free(my_ints) # << who frees this allocated piece? Is it rust, or is it leak'ed???
    return cp


def hencode(arr):
    cdef Array coords = _convert(arr)
    cdef size_t result = rs_hencode(coords)
    rs_array_free(coords)
    free(coords.data) # << NEEDED?
    return result


def hdecode(size_t key, size_t dims):
    cdef size_t i
    cdef Array result = rs_hdecode(key, dims)
    cdef size_t* incoming_ptr = <size_t*>(result.data)
    cdef size_t[:, :] view = <size_t[:result.len, :1]>incoming_ptr
    val = [view[i, 0] for i in range(result.len)]
    rs_array_free(result)
    free(result.data) # << NEEDED?!
    return val


def nencode(arr):
    cdef Array coords = _convert(arr)
    cdef size_t result = rs_nencode(coords)
    rs_array_free(coords)
    free(coords.data) # << NEEDED?
    return result


def ndecode(size_t key, size_t dims):
    cdef size_t i
    cdef Array result = rs_ndecode(key, dims)
    cdef size_t* incoming_ptr = <size_t*>(result.data)
    cdef size_t[:, :] view = <size_t[:result.len, :1]> incoming_ptr
    val = [view[i, 0] for i in range(result.len)]
    rs_array_free(result)
    free(result.data) # << NEEDED?!
    return val


def hquery(low, high, maxdepth = 10):
    cdef size_t y
    assert len(low) == len(high)
    cdef Array c0 = _convert(low)
    cdef Array c1 = _convert(high)
    cdef Array result = rs_hquery(c0, c1, maxdepth)
    cdef size_t* incoming_ptr = <size_t*>(result.data)
    cdef size_t[:, :] view = <size_t[:result.len, :2]> incoming_ptr
    val = zip([view[i, 0] for i in range(result.len)], 
              [view[i, 1] for i in range(result.len)])
    rs_array_free(result)
    free(result.data)
    return val


def nquery(low, high, maxdepth = 10):
    cdef size_t y
    assert len(low) == len(high)
    cdef Array c0 = _convert(low)
    cdef Array c1 = _convert(high)
    cdef Array result = rs_nquery(c0, c1, maxdepth)
    cdef size_t* incoming_ptr = <size_t*>(result.data)
    cdef size_t[:, :] view = <size_t[:result.len, :2]> incoming_ptr
    val = zip([view[i, 0] for i in range(result.len)], 
              [view[i, 1] for i in range(result.len)])
    rs_array_free(result)
    free(result.data)
    return val


#if __name__ == "__main__":
#    do(1)
