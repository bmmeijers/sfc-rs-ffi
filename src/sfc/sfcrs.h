#ifndef SFCRS_H
#define SFCRS_H

typedef struct Array {
    void* data;
    size_t len;
} Array;

Array hdecode(size_t input, size_t dims);
size_t hencode(Array input);

Array ndecode(size_t input, size_t dims);
size_t nencode(Array input);

void array_free(Array arr);

Array hquery(Array low, Array high, size_t maxdepth);
Array nquery(Array low, Array high, size_t maxdepth);

#endif
